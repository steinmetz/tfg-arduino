#ifndef D_ULTRASONICO_H
#define D_ULTRASONICO_H

#include "Arduino.h"


class Ultrassonico
{
private:        
    int trig, echo;
public:
	Ultrassonico(int trig, int echo);   
    int distancia();    
};

#endif
