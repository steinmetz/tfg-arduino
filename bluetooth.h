#ifndef D_BLUETOOTH_H
#define D_BLUETOOTH_H

#include "Arduino.h"
#include <HardwareSerial.h>

class Bluetooth
{
private:        
    HardwareSerial * serial;
public:
	Bluetooth(HardwareSerial * _serial);   
    void enviar(char *_str);    
    char *ler();    
};

#endif
