#ifndef D_L298N_H
#define D_L298N_H

#include "Arduino.h"

// default min and max speed
#define VELOCIDADE_MIN 64
#define VELOCIDADE_MAX 255

class L298N
{
private:        
    int en1, en2, en3, en4, ena, enb;
public:
	L298N(int en1, int en2, int en3, int en4, int ena, int enb);   
    void frente(int, int);
    void tras(int, int);
    void esquerda(int, int);
    void direita(int, int);
    void parar(void);
    boolean velocidade(int, int);    
};

#endif
