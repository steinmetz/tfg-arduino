#include "L298N.h"
#include "ultrassonico.h"
#include <SoftwareSerial.h>
#include <Servo.h>

void novoSensor(char* nome, int valor, int valorAnterior, int margem);
void updateSensorValue(char *nome, int valor);
int mudouValoresSensores();
char* getStringValores();

SoftwareSerial mySerial(3, 2); // RX, TX

L298N *ponteH;
Ultrassonico *ultrassonico_frente;
Ultrassonico *ultrassonico_tras;
Ultrassonico *ultrassonico_direita;
Ultrassonico *ultrassonico_esquerda;

Servo motor_frente;

/*
Type para armazanar valores dos sensores
*/
typedef struct cv{
    cv* prox;
    cv* ant;
    char* nome;
    int valor;
    int valorAnterior;
    int margem; //usado para ver se o valor mudou, dentro de uma margem
}ContentValues;

ContentValues *sensores = NULL;


/* Variáveis utilizadas para receber informações */
char d;
char receive_str[50];
char *part;
char c;

char *result;
    

/*Controle*/
char acao;
int angulo = 90;
int velocidade = 200;

int tempoSleep = 0;

int curva = 0; // direita
 

void setup()
{    
    ponteH = new L298N(42,43,44,45,4,5);

    ultrassonico_frente = new Ultrassonico(48,49);
    ultrassonico_tras = new Ultrassonico(50,51);
    ultrassonico_esquerda = new Ultrassonico(40,41);   
    ultrassonico_direita = new Ultrassonico(38,39);

    motor_frente.attach(3);

    motor_frente.write(angulo); 
    delay(500); 

    Serial.begin(9600); 
    Serial3.begin(9600);

    novoSensor("UE",0,0,5);
    novoSensor("UD",0,0,5);
    novoSensor("UF",0,0,5);
    novoSensor("UT",0,0,5);
    novoSensor("A",0,0,0);
    novoSensor("S",angulo,angulo,3);
    
    result =(char*) malloc(sizeof(char)*50);

    part = (char*) malloc(sizeof(char*)*10); 
    pinMode(13, OUTPUT);  
}




void loop() { 

 
        

    int vel = 66;

    int distancia = ultrassonico_frente->distancia();

 
    updateSensorValue("UF", distancia);
    if(distancia<20 && acao == 'F'){
        ponteH->parar();
    } 

    distancia = 6;// ultrassonico_direita->distancia();
    updateSensorValue("UD", distancia);

    distancia = ultrassonico_esquerda->distancia();
    updateSensorValue("UE", distancia);

    distancia = ultrassonico_tras->distancia();
    updateSensorValue("UT", distancia);

    updateSensorValue("S", angulo);

    if(mudouValoresSensores()>0){
        Serial.print(getStringValores()); 
        Serial3.print(getStringValores()); 
        // delay(10);
    } 
    
    


    if (Serial3.available() > 0){       
        c = Serial3.read(); 
        sprintf(part, "%c", c);     
        strcat(receive_str,part);        
        if(c=='#'){  
            if(strlen(receive_str)>5){

                int tamanho = strlen(receive_str);
                int i,j,k;
                int int_check=0;
                char char_check[50]; 
                char dados[tamanho-7];

                strcpy(char_check,"");
                strcpy(dados,"");


                for(j=1; receive_str[j] != '*'; j++){
                    sprintf(part, "%c", receive_str[j]);
                    strcat(char_check,part);
                }                       
                int_check = atoi(char_check);
                // Verificar se o valor no inicio da string 
                // é o mesmo que o tamanho dela
                if(int_check == tamanho){
                    Serial.print("String veio certa: ");
                    Serial.println(receive_str);

                    for(i=j+1, k=0; receive_str[i] != '\0'; i++, k++){
                        Serial.println(receive_str[i]);
                        if(receive_str[i]=='V'){
                            strcpy(char_check,"");
                            strcpy(part,"");
                            for(j=i+2; receive_str[j] != ';'; j++){
                                sprintf(part, "%c", receive_str[j]);
                                strcat(char_check,part);
                            }
                            velocidade = atoi(char_check);
                        } else if(receive_str[i]=='A'){

                            if(receive_str[i+2]=='P'){
                                acao = 'P';
                                updateSensorValue("A", (int)'P');
                                Serial.print("Parar!");
                                ponteH->parar();
                            }else if(receive_str[i+2]=='F'){
                                acao = 'F';
                                updateSensorValue("A", (int)'F');
                                Serial.print("Andar para frente!");
                                ponteH->frente(velocidade,velocidade);
                                if(tempoSleep > 0){
                                    delay(tempoSleep);
                                    tempoSleep = 0;    
                                    ponteH->parar();
                                }
                            }else if(receive_str[i+2]=='T'){
                                acao = 'T';
                                updateSensorValue("A", (int)'T');                                
                                Serial.print("Andar para trás!");
                                ponteH->tras(velocidade,velocidade);
                                if(tempoSleep > 0){
                                    delay(tempoSleep);
                                    tempoSleep = 0;    
                                    ponteH->parar();
                                }
                            }else if(receive_str[i+2]=='D'){
                                acao = 'D';
                                updateSensorValue("A", (int)'D');
                                Serial.print("Andar para direita!");
                                ponteH->direita(velocidade,velocidade);
                                if(tempoSleep > 0){
                                    delay(tempoSleep);
                                    tempoSleep = 0;    
                                    ponteH->parar();
                                }
                            }else if(receive_str[i+2]=='E'){
                                acao = 'E';
                                updateSensorValue("A", (int)'E');
                                Serial.print("Andar para esquerda!");
                                ponteH->esquerda(velocidade,velocidade);
                                if(tempoSleep > 0){
                                    delay(tempoSleep);
                                    tempoSleep = 0;    
                                    ponteH->parar();
                                }
                            }                            
                        } else if(receive_str[i]=='T'){
                            strcpy(char_check,"");
                            strcpy(part,"");
                            for(j=i+2; receive_str[j] != ';'; j++){
                                sprintf(part, "%c", receive_str[j]);
                                strcat(char_check,part);
                            }                   
                            if(atoi(char_check)>0){
                                tempoSleep = atoi(char_check);
                            }

                        }else if(receive_str[i]=='S'){
                            strcpy(char_check,"");
                            strcpy(part,"");
                            for(j=i+2; receive_str[j] != ';'; j++){
                                sprintf(part, "%c", receive_str[j]);
                                strcat(char_check,part);
                            }
                            angulo = atoi(char_check);
                            Serial.print("Girar servo:");
                            Serial.print(angulo);
                            Serial.println();
                            motor_frente.write(angulo);
                        }
                }
            }else{
                Serial.println("String veio ERRADA!");    
                Serial.println(receive_str);
                strcpy(receive_str, "");
                Serial.print("Tamanho da String:");
                Serial.print(tamanho);
                Serial.print("  Tamanho Check:");
                Serial.print(int_check);
            }
        }
        strcpy(receive_str, "");
    }
    }  
    delay(50); 

}    

void novoSensor(char* nome, int valor, int valorAnterior, int margem){
    ContentValues *novo;
    novo = (ContentValues*) malloc(sizeof(ContentValues));
    novo->nome = nome;
    novo->valor = valor;
    novo->valorAnterior = valorAnterior;
    novo->margem = margem;
    novo->ant = NULL;
    novo->prox = NULL; 

    if(sensores == NULL){
        sensores = novo;
    }else{
        while(sensores->prox){
            sensores = sensores->prox;
        }
        sensores->prox = novo;
        novo->ant = sensores;
    }
    while(sensores->ant){
        sensores = sensores->ant;
    }
}

void updateSensorValue(char *nome, int valor){
    if(sensores == NULL){
        return;
    }
    while(sensores->ant) sensores = sensores->ant;
    while(sensores->prox){
        if(strcmp(sensores->nome,nome)==0){           
            sensores->valor = valor;            
            break;
        }
        sensores = sensores->prox;
    }
    if(strcmp(sensores->nome,nome)==0){
        sensores->valor = valor;
    }
    
}
int mudouValoresSensores(){
    if(sensores == NULL){
        return 0;
    }
    ContentValues *l;
    
    l = sensores;
    while(l->ant) l = l->ant;
    while(l){    
        if( (l->valor > (l->valorAnterior+l->margem)) ||  (l->valor < (l->valorAnterior-l->margem)) ){
            return 1;
        }
        l=l->prox;
    }
    return 0;
}
char* getStringValores(){     
    char *aux;
    int count=0;
    char *check;
    char *valor;

    aux = (char*) malloc(sizeof(char)*40);
    check = (char*) malloc(sizeof(char)*2);
    valor = (char*) malloc(sizeof(char)*3);

    if(sensores == NULL){
        Serial.println("sensores nulo");
        return "";
    }
    strcpy(result, "$");
    while(sensores->ant) sensores = sensores->ant;
    strcpy(aux, "");
    while(sensores->prox){
        strcat(aux,sensores->nome);
        strcat(aux,":");
        sprintf(valor, "%d", sensores->valor);   
        strcat(aux,valor);
        strcat(aux,";");
        sensores->valorAnterior = sensores->valor;
        sensores = sensores->prox;
    }
    strcat(aux,sensores->nome);
    strcat(aux,":");
    sprintf(valor, "%d", sensores->valor);   
    strcat(aux,valor);
    strcat(aux,";");
    sensores->valorAnterior = sensores->valor;

    count = strlen(aux) + 4;
    sprintf(check, "%d", count);   
    strcat(result,check); 
    strcat(result,"*");
    strcat(result,aux);
    strcat(result,"#");

    free(aux);
    free(check);
    free(valor);

    return result;
}
