#include "L298N.h"

L298N::L298N(
  int _en1,
  int _en2,
  int _en3,
  int _en4,
  int _ena,
  int _enb)
{ 
  en1 = _en1;
  en2 = _en2;
  en3 = _en3;
  en4 = _en4;
  ena = _ena;
  enb = _enb;

  pinMode(en1, OUTPUT);
  pinMode(en2, OUTPUT);
  pinMode(en3, OUTPUT);
  pinMode(en4, OUTPUT);
  pinMode(ena, OUTPUT);
  pinMode(enb, OUTPUT);
}


void L298N::esquerda(int a, int b)
{ 
  digitalWrite(en3, HIGH);
  digitalWrite(en1, HIGH);
  digitalWrite(en4, LOW);
  digitalWrite(en2, LOW);
  velocidade(a, b);
}

void L298N::direita(int a, int b)
{
  digitalWrite(en4, HIGH);
  digitalWrite(en2, HIGH);
  digitalWrite(en3, LOW);
  digitalWrite(en1, LOW);
  velocidade(a, b);
}

void L298N::frente(int a, int b)
{ 
  digitalWrite(en3, LOW);
  digitalWrite(en1, HIGH);
  digitalWrite(en4, HIGH);
  digitalWrite(en2, LOW);
  velocidade(a, b); 
}
 
void L298N::tras(int a, int b)
{
  digitalWrite(en3, HIGH);
  digitalWrite(en1, LOW);
  digitalWrite(en4, LOW);
  digitalWrite(en2, HIGH);
  velocidade(a, b);
}

void L298N::parar()
{ 
  digitalWrite(en1, LOW);
  digitalWrite(en2, LOW);
  digitalWrite(en3, LOW);
  digitalWrite(en4, LOW);

  analogWrite(ena, 0);
  analogWrite(enb, 0);
}


boolean L298N::velocidade(int a, int b) {

  if ((a >= VELOCIDADE_MIN) && (a <= VELOCIDADE_MAX) && (b >= VELOCIDADE_MIN) && (b <= VELOCIDADE_MAX) )
  { 
    analogWrite(ena, a+40);
    analogWrite(enb, b+40);
    delay(100);
    analogWrite(ena, 0);
    analogWrite(enb, 0);
    analogWrite(ena, a);
    analogWrite(enb, b);
    return true;
  }
  return false;
}

