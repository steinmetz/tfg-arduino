#include "ultrassonico.h"

Ultrassonico::Ultrassonico(int _trig,int _echo ){ 
  trig = _trig;
  echo = _echo;

  pinMode(trig, OUTPUT);
  pinMode(echo,INPUT);
}

int Ultrassonico::distancia(){ 

  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  
  unsigned long duracao = pulseIn(echo, HIGH);
  int distancia = duracao / 58;
  return distancia;
}