#ifndef D_GPS_H
#define D_GPS_H

#include <Arduino.h> 
#include <SoftwareSerial.h>

class GPS
{
public:        
    float latitude, longitude; 
	SoftwareSerial * gpsSerial; 
	GPS(SoftwareSerial * serial);
	void refresh();
};

#endif
